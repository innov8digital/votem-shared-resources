
class validation{

    static validationType = {
        length: {
            meta: 'value is within predefined length',
            eval: (validationTarget = '', definedLength = 10) => !!validationTarget && validationTarget.length <= definedLength
        },
        presence: {
            meta: 'value present and not empty, null, or undefined',
            eval: (validationTarget) => !!validationTarget
        },
        range: {
            meta: 'value evaluation within defined range',
            eval: (validationTarget) => !!validationTarget

        },
        format: 'value is valid format'

    }


    static validationLevel = {
        warning: 'warning',
        error: 'error',
        fatal: 'fatal'
    }

    constructor(validationType, validationLevel, validationMessage){
        this.type = !validationType ?
            validationType ||
            validation.validationType.presence;
        this.level = !validationLevel ?
            validationLevel ||
            validation.validationLevel.warning;
        this.validationMessage = !validationMessage ?
            validationMessage ||
            'Please complete all required fields';
        this.shouldDisplayError = this.level != validation.validationLevel.warning;
    }

    validate(validationTarget){
        if(this.type.eval(validationTarget) && this.shouldDisplayError)
            this.showErrorMessage();
    }

    showErrorMessage(){

    }

    hideErrorMessage(){

    }
}